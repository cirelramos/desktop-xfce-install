INSTALL DESKTOP XFCE WITH CONFIG SAVED
============






Requisites
----------------
XFCE UBUNTU 16.04 OR LINUX MINT XFCE

INSTALL
-------

1. Run the quick install
```shell
sudo wget https://gitlab.com/cirelramos/desktop-xfce-install/raw/master/quick-install.sh  -O - | sudo bash
```

2. restart your computer


3. terminal --hide-borders --hide-scrollbar --fullscreen --hide-menubar
```shell
xfce4-terminal --hide-borders --hide-scrollbar --fullscreen --hide-menubar
``` 
