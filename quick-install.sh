#!/bin/bash
userLog=$(logname)
if [ "$userLog"=="" ]; then
	userLog=$(w | head -n3 | tail -n 1 | cut -d " " -f1)
fi
echo "==============================="$userLog"==============================="
rute="/home/$userLog"
rm -r /opt/install-desktop/
git clone https://gitlab.com/cirelramos/desktop-xfce-install.git /opt/install-desktop
cd /opt/install-desktop
egrep -lRZ '/cirel/' . | xargs -0 -l sed -i -e 's/cirel/$userLog/g'
cp -arf .conky $rute/
cp -arf wallpaper.png $rute/Pictures/
cp -arf xfce4 $rute/.config/
cp -arf autostart $rute/.config/
cp -arf conky-manager.json $rute/.config/
rm /usr/share/icons/default/index.theme
cp -arf nitro-tech/* /usr/share/icons/default/
if [ ! -f "/usr/bin/conky-manager" ]; then
	apt-add-repository -y ppa:teejee2008/ppa
	apt-get update
	apt-get install conky-manager -y
	dpkg-reconfigure hddtemp
fi
chown $userLog:$userLog -R $rute/.conky $rute/.config $rute/.fonts
